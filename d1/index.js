console.log("Hello World");

//Arithmetic Operators

	let x = 1397;
	let y = 7831;

	//Sum
	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	//Difference
	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	//Product
	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	//Division
	let qoutient = x / y;
	console.log("Result of division operator: " + qoutient);

	//Modulo
	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);



//Assignment Operators (=)
	//Basic Assignment Operator
	
	let assignmentNumber = 8;

	//Addition Assignment Operator
	//Addition Assignment Operator adds the value of the right operand to a variable and assigns the result to the variable.

	//long hand
	assignmentNumber = assignmentNumber + 2;
	console.log("Result of addition operator: " + assignmentNumber);
	
	//short hand
	assignmentNumber += 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);
	//The same lang silang dalawa.


//Subtraction Multiplication Division Assignment Operator (-=, *=, /=)

	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " + assignmentNumber);




//PEMDAS

//multiple operators and parentheses
	/*
		-when multiple operators are applied in a single statement. it follows the PEMDAS. (Parentheses, Exponents, Multiplication, Division, Addition, and Subtraction) rule.
		1.	3 * 4	= 12
		2.	12 / 5	= 2.4
		3.	1 + 2 	= 3
		4.	3 - 2.4	= 0.6
	*/

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas operator: " + mdas);

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of pemdas operator: " + pemdas);

	/*
		-by adding parentheses "()" to create more complex computations will change the order of operations still follows the same rule.
		1.	4/5 = 0.8
		2.	2-3 = -1
		3.	-1*0.8 = -0.8
		4.	1+-0.8 = .2
	*/



//Increament and Decrement
	//Operators that add or subtract value by 1 and reassigns the value of the variable where the increment or decrement was applied to.

	//pre-increment
	let z = 1;
	//the value of "z" is added by a value of one before returning the value and storing it in the variable "increment"
	let increment = ++z;
	console.log("Result of increment: " + increment);
	console.log("Result of z: " + z);

	//the value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by 1 

	//post-increment
	increament = z++;
	//here ang na increase lang ay si "z"
	console.log("Result of post increment: " + increment);
	console.log("Result of post increment: " + z);


	//pre-decrement
	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + z);

	//post-decrement
	decrement = z--;
	console.log("Result of post-decrement: " + decrement);
	console.log("Result of post-decrement: " + z);



//typeof Coercion
	
	let numA = "10";
	let numB = 12;

	/*
		-adding or concatenating a string and a number will result in a string.
		-this can vbe proven in the console by looking at the color of the text displayed.
	*/

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;

	/*
		-the result is a number
		-this can be proven in the console by looking at the color of the text.
		-blue text means the output returned is a number data type
	*/

	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);



	/*
		-the result is a number
		-the boolean  "true"  is also associated with the value of 1
	*/
	let numE = true + 1;
	console.log(numE);

	/*
		-the result is a number
		-the boolean  "false"  is also associated with value of 0.
	*/
	let numF = false + 1;
	console.log(numF);




//Comparison Operators

	let juan = "juan";

	//Equality Operator ( == )

	/*
		-checks wheter the operands are equal or have the same content.
		-this attempts to CONVERT and COMPARE operands of different data types
		-return a boolean value
	*/

	console.log(1 == 1);	//true
	console.log(1 == 2);	//false
	console.log(1 == "1");	//true
	console.log(0 == false);	//true
	console.log("juan" == "juan");	//true
	console.log("juan" == juan);	//true




//Inequality Operator

	/*
		-checks whether the operands are not equal or have different content.
		-attempts to CONVERT and COMPARE operands of different data type
	*/

	console.log(1 != 1);	//false
	console.log(1 != 2);	//true
	console.log(1 != "1");	//false
	console.log(0 != false);	//false
	console.log("juan" != "juan");	//false
	console.log("juan" != juan);	//false




//Strict Equality Operator ( === )
	/*
		-it checks whether the operands are equal or have the same content
		-also it COMPARES the data types of 2 values.
		-JS is a loosely type language meaning that values of different data types can be stored in variable.
		-strict equality operators are better to use in must cases to ensure that data types are correct.
	*/

	console.log(1 === 1);	//true
	console.log(1 === 2);	//false
	console.log(1 === "1");	//false
	console.log(0 === false);	//false
	console.log("juan" === "juan");	//true
	console.log("juan" === juan);	//true





//Strict Inequality Operator ( !== )

	/*
		-it checks whether the operands are not equal or have the same content
		-also COMPARES the date types of 2 value
	*/

	console.log(1 !== 1);	//false
	console.log(1 !== 2);	//true
	console.log(1 !== "1");	//true
	console.log(0 !== false);	//true
	console.log("juan" !== "juan");	//false
	console.log("juan" !== juan);	//false




//Relational Operators
	/*
		-
	*/

	let a = 50;
	let b = 65;

	let isGreaterThan = a > b;
	let isLessThan = a < b;
	let isGTorEqual = a >= b;
	let isLTorEqual = a <= b;

	console.log(isGreaterThan);	//false
	console.log(isLessThan);	//true
	console.log(isGTorEqual);	//false
	console.log(isLTorEqual);	//true

	let numStr = "30";
	//coercion to change the string to a number
	console.log(a > numStr);	//true
	console.log(b <= numStr);	//false

	let str = "twenty";
	console.log(b >= str);	//false
	//since the string in not numeric.
	//the string was converted to a number.
	//result to NaN. 65 is not greater than NaN




//Logical Operator ( &&, ||, ! )

	let isLegalAge = true;
	let isRegistered = false;


	//Logical And Operator (&& - Double Ampersand)
	//Return True if all operand are true
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical AND operator: " + allRequirementsMet);	//false


	//Logical Or Operator (|| - Double Pipe)
	//Return TRUE if onw of the operands are true
	let someRequirementsNotMet = isLegalAge || isRegistered;
	console.log("Result of logical OR Operator: " + someRequirementsNotMet);	//true


	//Logical Not Operator (! - Exclamation Point)
	//Return the opposite value
	// let someRequirementsNotMet = !isRegistered;
	// console.log("Result of logical not operator" + someRequirementsNotMet);


//typeof operator used to check the data type of a value or expression and return a string value of what the data type is.